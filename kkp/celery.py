from __future__ import absolute_import, unicode_literals

import os

from celery import Celery

from problem_solver.tasks import find_problems

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'kkp.settings')
app = Celery('kkp')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    find_problems()
    # sender.add_periodic_task(3600, find_problems, name='finding problems every hour')
