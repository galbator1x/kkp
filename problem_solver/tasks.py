from celery import task

from problem_solver.utils import find_order_problems


@task
def find_problems():
    find_order_problems()
