import os

import django

os.environ['DJANGO_SETTINGS_MODULE'] = 'kkp.settings'
django.setup()

from .models import Order, OrderProblem


def find_order_problems():
    problems = []
    for order in Order.objects.filter(is_processed=False):
        order.calculate_and_set_norm_excess()

        # for worker in order.workers.all():
        #     today = datetime.now()
        #     if OrderProblem.objects.filter(added_at__year=today.year,
        #                                    added_at__month=today.month,
        #                                    order__workers__in=[worker]):
        #                            # .filter(order__workers__in=[worker]):
        #         print(f'{worker}')

        if order.is_high_norm_excess():
            problem = OrderProblem(importance='high', order=order)
            problems.append(problem)
            problem.save()
        elif order.is_low_norm_excess():
            # TODO
            pass
        order.is_processed = True
        order.save()
