from django.db import models
from django.utils import timezone


class Staff(models.Model):
    full_name = models.CharField(max_length=256)
    position = models.CharField(max_length=256)
    experience = models.IntegerField()

    def __str__(self):
        return self.full_name


class Raw(models.Model):
    name = models.CharField(max_length=256)
    measure = models.CharField(max_length=256)

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    amount = models.DecimalField(max_digits=15, decimal_places=3)
    raw = models.ForeignKey(Raw)

    def __str__(self):
        return str(self.raw)


class Dish(models.Model):
    name = models.CharField(max_length=256)
    composition = models.ManyToManyField(Ingredient)
    price = models.FloatField()
    cooking_time_seconds = models.IntegerField(default=0)
    cooking_time_minutes = models.IntegerField()

    def __str__(self):
        return self.name

    def get_cooking_time_seconds(self):
        return self.cooking_time_minutes * 60 + self.cooking_time_seconds


class OrderItem(models.Model):
    dish = models.ForeignKey(Dish)
    amount = models.IntegerField()

    def __str__(self):
        return str(self.dish)


class Order(models.Model):
    MIN_LOW_ERROR_LIMIT = 1.16
    MAX_LOW_ERROR_LIMIT = 1.30
    MIN_HIGH_ERROR_LIMIT = 1.31

    date = models.DateTimeField() # maybe redundant
    started_at = models.DateTimeField()
    ended_at = models.DateTimeField()
    workers = models.ManyToManyField(Staff)
    items = models.ManyToManyField(OrderItem)
    is_processed = models.BooleanField(default=False)
    norm_excess = models.FloatField(default=0, editable=False)

    def __str__(self):
        items = f'{str(self.date.strftime("%A, %d. %B %Y %I:%M%p"))} '
        for item in self.items.all():
            items += f'{str(item.amount)} {item.dish.name}, '
        return items[:-2]

    def get_lead_time_seconds(self):
        return (self.ended_at - self.started_at).total_seconds()

    def get_longest_cooking_dish(self):
        longest_order_item = max(self.items.all(),
                                 key=lambda item: item.dish.get_cooking_time_seconds())
        return longest_order_item.dish

    def is_low_norm_excess(self):
        return self.MIN_LOW_ERROR_LIMIT <= self.norm_excess >= self.MAX_LOW_ERROR_LIMIT

    def is_high_norm_excess(self):
        return self.norm_excess >= self.MIN_HIGH_ERROR_LIMIT

    def calculate_and_set_norm_excess(self):
        # TODO add check if items is not empty
        dish = self.get_longest_cooking_dish()
        norm_time = dish.get_cooking_time_seconds()
        self.norm_excess = self.get_lead_time_seconds() / norm_time


class WastedDishes(models.Model):
    dish = models.ForeignKey(Dish)
    amount = models.IntegerField()
    date = models.DateTimeField()

    def __str__(self):
        return str(self.dish)


class WastedRaws(models.Model):
    raw = models.ForeignKey(Raw)
    amount = models.IntegerField()
    date = models.DateTimeField()

    def __str__(self):
        return str(self.raw)


class RemainderRaws(models.Model):
    raw = models.ForeignKey(Raw)
    amount = models.IntegerField()
    date = models.DateTimeField()

    def __str__(self):
        return str(self.raw)


class Provider(models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return self.name


class Purchase(models.Model):
    provider = models.ForeignKey(Provider)
    price = models.FloatField()
    raw = models.ForeignKey(Raw)

    def __str__(self):
        return str(self.raw)


class Problem(models.Model):
    IMPORTANCE_CHOICES = (
        ('low', 'низкая'),
        ('high', 'высокая'),
    )

    importance = models.CharField(max_length=10, choices=IMPORTANCE_CHOICES,
                                  editable=False)
    added_at = models.DateTimeField(default=timezone.now)
    is_solved = models.BooleanField(default=False)

    class Meta:
        abstract = True


class OrderProblem(Problem):
    order = models.ForeignKey(Order)

    def __str__(self):
        return str(self.order)

    class Meta:
        verbose_name = 'Проблема времени выполнения'
        verbose_name_plural = 'Проблемы времени выполнения'
