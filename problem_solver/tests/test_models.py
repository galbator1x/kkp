import pytest
from datetime import timedelta
from django.utils import timezone
from model_mommy import mommy

from problem_solver.models import Dish, Order, OrderItem, OrderProblem


def test_dish_cooking_time_calculation():
    dish = mommy.prepare(Dish, cooking_time_seconds=5, cooking_time_minutes=1)

    assert dish.get_cooking_time_seconds() == 65

@pytest.mark.django_db
def test_lead_time_calculation():
    dt = timezone.now()
    order = mommy.make(Order, started_at=dt, ended_at=dt + timedelta(seconds=180))

    assert order.get_lead_time_seconds() == 180


@pytest.mark.django_db
def test_longest_cooking_dish_calculation():
    order = mommy.make(Order)
    item1 = mommy.make(OrderItem, dish=mommy.make(Dish, cooking_time_minutes=2))
    item2 = mommy.make(OrderItem, dish=mommy.make(Dish, cooking_time_minutes=3))
    order.items.add(item1, item2)
    longest_cooking_time = order.get_longest_cooking_dish().get_cooking_time_seconds()

    assert longest_cooking_time == 180


def test_excess_calculation():
    assert 1 == 1


# @pytest.mark.django_db
# def test_satisfying_norm():
#     dt = timezone.now()
#     order = mommy.make(Order, started_at=dt, ended_at=dt + timedelta(seconds=130))
#     item = mommy.make(OrderItem, dish=mommy.make(Dish, cooking_time_minutes=2,
#                                                  cooking_time_seconds=5,
#                                                  norm_cooking_time_minutes=2))
#     order.items.add(item)

#     assert order.is_satisfy_norm()


# @pytest.mark.django_db
# def test_not_satisfying_norm():
#     dt = timezone.now()
#     order = mommy.make(Order, started_at=dt, ended_at=dt + timedelta(seconds=190))
#     item = mommy.make(OrderItem, dish=mommy.make(Dish, cooking_time_minutes=3,
#                                                  norm_cooking_time_minutes=2))
#     order.items.add(item)

#     assert not order.is_satisfy_norm()
