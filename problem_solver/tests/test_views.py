import pytest

from problem_solver.views import index


@pytest.mark.django_db
class TestIndex():
    def test_index(self, rf):
        request = rf.get('index')
        response = index(request)
        assert response.status_code == 200
