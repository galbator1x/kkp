from django.contrib import admin

from .models import (Dish, Ingredient, Order, OrderItem, OrderProblem, Provider,
                     Purchase, Raw, RemainderRaws, Staff, WastedDishes, WastedRaws)

admin.site.register([Dish, Ingredient, Order, OrderItem, OrderProblem, Provider,
                    Purchase, Raw, RemainderRaws, Staff, WastedDishes, WastedRaws])
