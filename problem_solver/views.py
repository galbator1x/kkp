from django.shortcuts import render

from .models import Order, OrderProblem


def index(request):
    problems = OrderProblem.objects.filter(importance='high')
    # FIXME filter orders by last day
    orders = Order.objects.all()
    context = {
        'problems': problems,
        'orders': orders,
        'problem_verbose_name': OrderProblem._meta.verbose_name,
    }
    return render(request, 'problem_solver/index.html', context)
