from django.apps import AppConfig


class ProblemSolverConfig(AppConfig):
    name = 'problem_solver'
