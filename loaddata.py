import os
import django
import random
from faker import Faker

os.environ['DJANGO_SETTINGS_MODULE'] = 'kkp.settings'
django.setup()

from problem_solver.models import *
from model_mommy import mommy
from django.utils import timezone
from datetime import timedelta
import pytz


if __name__ == '__main__':
    fake = Faker()

    OrderProblem.objects.all().delete()

    Staff.objects.all().delete()
    positions = ['менеджер', 'повар', 'повар', 'кассир'] * 3
    workers = []
    for position in positions:
        workers.append(mommy.make(Staff, full_name=fake.name(), position=position,
                       experience=random.randrange(1, 20)))

    Raw.objects.all().delete()
    chicken_fillet = mommy.make(Raw, name='Куриное филе', measure='кг')
    cheese = mommy.make(Raw, name='Сыр', measure='кг')
    egg = mommy.make(Raw, name='Яйцо', measure='кг')
    tomato = mommy.make(Raw, name='Помидор', measure='кг')
    crackers = mommy.make(Raw, name='Сухарики', measure='кг')
    coffee = mommy.make(Raw, name='Кофе', measure='кг')
    # mommy.make(Raw, name='', measure='кг')

    Dish.objects.all().delete()
    caesar = mommy.make(Dish, name='Цезарь', price=335, cooking_time_minutes=10, make_m2m=True)
    dish1 = mommy.make(Dish, name='Чафан', price=210, cooking_time_minutes=7, make_m2m=True)
    dish2 = mommy.make(Dish, name='Салат греческий', price=215, cooking_time_minutes=5, make_m2m=True)
    dish3 = mommy.make(Dish, name='Борщ', price=175, cooking_time_minutes=15, make_m2m=True)
    dish4 = mommy.make(Dish, name='Крем-суп грибной', price=240, cooking_time_minutes=15, make_m2m=True)
    americano = mommy.make(Dish, name='Американо', price=90, cooking_time_minutes=1)
    americano.composition.add(mommy.make(Ingredient, raw=coffee, amount=0.003))
    dish5 = mommy.make(Dish, name='Капучино', price=125, cooking_time_minutes=1, make_m2m=True)
    dishes = [caesar, dish1, dish2, dish3, dish4, americano, dish5]

    OrderItem.objects.all().delete()
    Order.objects.all().delete()
    # orders count
    for _ in range(10):
        dt = fake.date_time_between(start_date='-60d', end_date='now', tzinfo=pytz.timezone('UTC'))
        order = mommy.make(Order, date=dt, started_at=dt) # TODO check if is processed false
        random.shuffle(dishes)
        for dish in dishes[:random.randrange(1,4)]:
            order.items.add(mommy.make(OrderItem, dish=dish, amount=random.randrange(1, 4)))
        longest_cooking_sec = order.get_longest_cooking_dish().get_cooking_time_seconds()
        lead_time = int(longest_cooking_sec * random.uniform(0.9, 1.6))
        order.ended_at = order.started_at + timedelta(seconds=lead_time)
        random.shuffle(workers)
        for worker in workers[:3]:
            order.workers.add(worker)
        order.save()

    WastedDishes.objects.all().delete()
    mommy.make(WastedDishes, dish=caesar, amount=15, date=timezone.now())

    WastedRaws.objects.all().delete()
    mommy.make(WastedRaws, raw=chicken_fillet, amount=20, date=timezone.now())

    RemainderRaws.objects.all().delete()
    mommy.make(RemainderRaws, raw=chicken_fillet, amount=12, date=timezone.now())

    Provider.objects.all().delete()
    Purchase.objects.all().delete()
    mommy.make(Purchase, raw=chicken_fillet, price=250, provider=mommy.make(Provider, name=fake.company()))
