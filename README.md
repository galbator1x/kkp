# Course project

## Installation

[install python 3.6](https://askubuntu.com/a/865569)

#### Install and activate virtual environment
```
$ python3.6 -m venv /path/to/new/virtual/environment
$ source /path/to/new/virtual/environment/bin/activate
```

#### Clone project, install requirements, create database
```
$ git clone https://galbator1x@bitbucket.org/galbator1x/kkp.git
$ cd kkp
$ pip install -r requirements.txt
$ python manage.py migrate
$ python manage.py makemigrations problem_solver
$ python manage.py migrate
$ python manage.py createsuperuser
```

#### Install RabbitMQ
```
$ sudo apt-get install rabbitmq-server
```

#### Load test data into database
```
$ python loaddata.py
```

## Running the tests

```
$ pytest
```

## Usage

#### Activate virtual environment, run RabbmitMQ server, celery worker and project server:
```
$ invoke-rc.d rabbitmq-server start
$ celery -A kkp worker -l info -B
$ python manage.py runserver
```
